package cz.cvut.fel.dbs2023;

import cz.cvut.fel.dbs2023.DAO.BankAccountDAO;
import cz.cvut.fel.dbs2023.DAO.ServicesDAO;
import cz.cvut.fel.dbs2023.Entities.BankAccount;
import cz.cvut.fel.dbs2023.Entities.Services;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.HashSet;
import java.util.Set;

public class ServicesAndBankAccount {
    public void start(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("dbs2023-persistence-unit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        BankAccountDAO bankAccountDAO = new BankAccountDAO(entityManager);
        ServicesDAO servicesDAO = new ServicesDAO(entityManager);

        // Start of transaction
        EntityTransaction transaction = null;
        transaction = entityManager.getTransaction();

        transaction.begin();

        try {
            // 1st Bank Account
            BankAccount bankAccount1 = new BankAccount();
            bankAccount1.setBalance(1000);
            bankAccount1.setType("Standard");
            bankAccountDAO.create(bankAccount1);
            System.out.println("A new Bank Account record has been created: " + bankAccount1);

            // 2nd Bank Account
            BankAccount bankAccount2 = new BankAccount();
            bankAccount2.setBalance(1500);
            bankAccount2.setType("Visa");
            bankAccountDAO.create(bankAccount2);
            System.out.println("A new Bank Account record has been created: " + bankAccount2);

            // 3rd Bank Account
            BankAccount bankAccount3 = new BankAccount();
            bankAccount3.setBalance(3500);
            bankAccount3.setType("Moderate");
            bankAccountDAO.create(bankAccount3);
            System.out.println("A new Bank Account record has been created: " + bankAccount3);

            // Set of Bank Accounts
            Set<BankAccount> accountSet = new HashSet<>();
            accountSet.add(bankAccount1);
            accountSet.add(bankAccount2);
            accountSet.add(bankAccount3);

            // Services
            Services services = new Services();
            services.setServiceNum(123);
            services.setPeriod(95);
            services.setBankAccounts(accountSet);
            servicesDAO.create(services);
            System.out.println("A new Services record has been created: " + services);

            // Commit the transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            // Close the EntityManager and EntityManagerFactory
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
