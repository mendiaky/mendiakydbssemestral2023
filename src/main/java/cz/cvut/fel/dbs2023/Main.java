package cz.cvut.fel.dbs2023;

import cz.cvut.fel.dbs2023.DAO.AddressDAO;
import cz.cvut.fel.dbs2023.DAO.BankAccountDAO;
import cz.cvut.fel.dbs2023.DAO.BranchOfficeDAO;
import cz.cvut.fel.dbs2023.DAO.CustomerDAO;
import cz.cvut.fel.dbs2023.Entities.Address;
import cz.cvut.fel.dbs2023.Entities.BankAccount;
import cz.cvut.fel.dbs2023.Entities.BranchOffice;
import cz.cvut.fel.dbs2023.Entities.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Main {
    public static void main(String[] args) {

        //Starting operations with customer
        CustomerScenario customerScenario = new CustomerScenario();
        customerScenario.start();

        //Starting operations with bankAccount
        BankAccountScenario bankAccountScenario = new BankAccountScenario();
        bankAccountScenario.start();

        //Starting operations with Address and BranchOffice
        AddressAndBranchOfficeScenario abs = new AddressAndBranchOfficeScenario();
        abs.start();

        ServicesAndBankAccount servicesAndBankAccount = new ServicesAndBankAccount();
        servicesAndBankAccount.start();


    }
}