package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.Address;
import cz.cvut.fel.dbs2023.Entities.AddressId;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

public class AddressDAO {
    private EntityManagerFactory entityManagerFactory;

    public AddressDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void create(Address address) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            entityManager.persist(address);
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public void update(Address address) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            entityManager.merge(address);
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public void delete(Address address) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            entityManager.remove(entityManager.merge(address));
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public Address findById(String street, String city) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Address address = null;

        try {
            address = entityManager.find(Address.class, new AddressId(street, city));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
        }

        return address;
    }

    public List<Address> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Address> addresses = null;

        try {
            addresses = entityManager.createQuery("SELECT a FROM Address a").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
        }

        return addresses;
    }
}
