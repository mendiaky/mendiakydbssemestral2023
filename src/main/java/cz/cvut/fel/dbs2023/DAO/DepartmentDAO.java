package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.Department;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class DepartmentDAO {
    private EntityManager entityManager;

    public DepartmentDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void create(Department department) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(department);
        transaction.commit();
    }

    public Department findById(int departmentID) {
        return entityManager.find(Department.class, departmentID);
    }

    public List<Department> findAll() {
        Query query = entityManager.createQuery("SELECT d FROM Department d");
        return query.getResultList();
    }

    public void update(Department department) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(department);
        transaction.commit();
    }

    public void delete(Department department) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.remove(department);
        transaction.commit();
    }
}
