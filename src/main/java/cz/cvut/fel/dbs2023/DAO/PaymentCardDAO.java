package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.PaymentCard;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class PaymentCardDAO {
    private EntityManager entityManager;

    public PaymentCardDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void create(PaymentCard paymentCard) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(paymentCard);
        transaction.commit();
    }

    public PaymentCard findById(int cardNumber) {
        return entityManager.find(PaymentCard.class, cardNumber);
    }

    public List<PaymentCard> findAll() {
        Query query = entityManager.createQuery("SELECT p FROM PaymentCard p");
        return query.getResultList();
    }

    public void update(PaymentCard paymentCard) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(paymentCard);
        transaction.commit();
    }

    public void delete(PaymentCard paymentCard) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.remove(paymentCard);
        transaction.commit();
    }
}
