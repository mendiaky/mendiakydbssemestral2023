package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.Services;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class ServicesDAO {
    private EntityManager entityManager;

    public ServicesDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void create(Services services) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.persist(services);
    }

    public Services findById(int serviceNum) {
        return entityManager.find(Services.class, serviceNum);
    }

    public List<Services> findAll() {
        Query query = entityManager.createQuery("SELECT s FROM Services s");
        return query.getResultList();
    }

    public void update(Services services) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.merge(services);
    }


    public void delete(Services services) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.remove(services);
    }
}
