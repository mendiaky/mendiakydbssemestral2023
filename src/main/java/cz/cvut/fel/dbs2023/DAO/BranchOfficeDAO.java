package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.BranchOffice;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

public class BranchOfficeDAO {
    private EntityManagerFactory entityManagerFactory;

    public BranchOfficeDAO(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void create(BranchOffice branchOffice) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            entityManager.merge(branchOffice);
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public void update(BranchOffice branchOffice) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            entityManager.merge(branchOffice);
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public void delete(BranchOffice branchOffice) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            entityManager.remove(entityManager.merge(branchOffice));
            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public BranchOffice findById(int branchNum) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        BranchOffice branchOffice = null;

        try {
            branchOffice = entityManager.find(BranchOffice.class, branchNum);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
        }

        return branchOffice;
    }

    public List<BranchOffice> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<BranchOffice> branchOffices = null;

        try {
            branchOffices = entityManager.createQuery("SELECT bo FROM BranchOffice bo").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
        }

        return branchOffices;
    }
}
