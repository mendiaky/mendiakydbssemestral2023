package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.Deposit;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class DepositDAO {
    private EntityManager entityManager;

    public DepositDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void create(Deposit deposit) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(deposit);
        transaction.commit();
    }

    public Deposit findById(int depositNumber) {
        return entityManager.find(Deposit.class, depositNumber);
    }

    public List<Deposit> findAll() {
        Query query = entityManager.createQuery("SELECT d FROM Deposit d");
        return query.getResultList();
    }

    public void update(Deposit deposit) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(deposit);
        transaction.commit();
    }

    public void delete(Deposit deposit) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.remove(deposit);
        transaction.commit();
    }
}

