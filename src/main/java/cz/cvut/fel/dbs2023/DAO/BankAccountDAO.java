package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.BankAccount;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class BankAccountDAO {
    private EntityManager entityManager;

    public BankAccountDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void create(BankAccount bankAccount) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.persist(bankAccount);
    }

    public BankAccount findById(int accountNumber) {
        return entityManager.find(BankAccount.class, accountNumber);
    }

    public List<BankAccount> findAll() {
        Query query = entityManager.createQuery("SELECT b FROM BankAccount b");
        return query.getResultList();
    }

    public void update(BankAccount bankAccount) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.merge(bankAccount);
    }

    public void delete(BankAccount bankAccount) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.remove(bankAccount);
    }

}
