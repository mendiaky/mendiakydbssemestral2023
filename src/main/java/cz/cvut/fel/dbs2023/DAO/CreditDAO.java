package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.Credit;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class CreditDAO {
    private EntityManager entityManager;

    public CreditDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void create(Credit credit) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.persist(credit);
    }

    public Credit findById(int contractNumber) {
        return entityManager.find(Credit.class, contractNumber);
    }

    public List<Credit> findAll() {
        Query query = entityManager.createQuery("SELECT c FROM Credit c");
        return query.getResultList();
    }

    public void update(Credit credit) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.merge(credit);
    }

    public void delete(Credit credit) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.remove(credit);
    }
}
