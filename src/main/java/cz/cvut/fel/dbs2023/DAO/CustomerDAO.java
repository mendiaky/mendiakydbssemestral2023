package cz.cvut.fel.dbs2023.DAO;

import cz.cvut.fel.dbs2023.Entities.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class CustomerDAO {
    private EntityManager entityManager;

    public CustomerDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void create(Customer customer) {
        entityManager.persist(customer);
    }

    public Customer findById(int customerID) {
        return entityManager.find(Customer.class, customerID);
    }

    public List<Customer> findAll() {
        Query query = entityManager.createQuery("SELECT c FROM Customer c");
        return query.getResultList();
    }

    public void update(Customer customer) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.merge(customer);
    }

    public void delete(Customer customer) {
        EntityTransaction transaction = entityManager.getTransaction();
        entityManager.remove(customer);
    }
}
