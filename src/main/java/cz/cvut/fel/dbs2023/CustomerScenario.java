package cz.cvut.fel.dbs2023;

import cz.cvut.fel.dbs2023.DAO.CustomerDAO;
import cz.cvut.fel.dbs2023.Entities.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class CustomerScenario {

    public void start(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("dbs2023-persistence-unit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            CustomerDAO customerDAO = new CustomerDAO(entityManager);


            // Create a new record for the Customer entity

            Customer customer = new Customer();
            customer.setEmail("orlando.morgan@example.com");
            customer.setName("Orlando Morgan");
            customer.setPhone("123456789");
            customer.setAge(26);
            customerDAO.create(customer);
            System.out.println("A new Customer record has been created: " + customer);


            // Finding a Customer record by ID
            Customer foundCustomer = customerDAO.findById(customer.getCustomerID());
            System.out.println("Found record Customer: " + foundCustomer);

            // Update a customer record
            foundCustomer.setName("Updated Name");
            customerDAO.update(foundCustomer);
            System.out.println("Updated the Customer record: " + foundCustomer);

            // Deleting a Customer Record
            customerDAO.delete(foundCustomer);
            System.out.println("Removed Customer entry: " + foundCustomer);


            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }
}
