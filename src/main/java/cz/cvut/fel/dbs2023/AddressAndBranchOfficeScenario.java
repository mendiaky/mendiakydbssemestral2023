package cz.cvut.fel.dbs2023;

import cz.cvut.fel.dbs2023.DAO.AddressDAO;
import cz.cvut.fel.dbs2023.DAO.BranchOfficeDAO;
import cz.cvut.fel.dbs2023.Entities.Address;
import cz.cvut.fel.dbs2023.Entities.BranchOffice;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AddressAndBranchOfficeScenario {



    public void start(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("dbs2023-persistence-unit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        // Creating DAO Instances
        AddressDAO addressDAO = new AddressDAO(entityManagerFactory);
        BranchOfficeDAO branchOfficeDAO = new BranchOfficeDAO(entityManagerFactory);

        // Create and save a new Address object
        Address address = new Address();
        address.setStreet("st. Sandreland");
        address.setCity("New York");
        address.setPostCode("143244");

        addressDAO.create(address);
        System.out.println("Created a new Address with street: " + address.getStreet() + " и городом: " + address.getCity());

        // Create and save a new BranchOffice object with the addition of Address
        BranchOffice branchOffice = new BranchOffice();
        branchOffice.setCapacity(300);
        branchOffice.setAddress(address);
        branchOffice.setBranchNum(10);

        branchOfficeDAO.create(branchOffice);
        System.out.println("Created a new BranchOffice with branchNum: " + branchOffice.getBranchNum());

        // Closing EntityManagerFactory
        entityManagerFactory.close();
    }
}
