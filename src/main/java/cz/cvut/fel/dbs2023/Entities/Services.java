package cz.cvut.fel.dbs2023.Entities;
import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Entity
@Table(name = "services")
@DiscriminatorColumn(name = "SERVICE")
public class Services {
    @Id
    @Column(name = "serviceNum")
    private int serviceNum;

    @Column(name = "period", nullable = false)
    private int period;

    @ManyToOne
    @JoinColumn(name = "branchOffice", referencedColumnName = "BranchNum")
    private BranchOffice branchOffice;

    @ManyToOne
    @JoinColumn(name = "customer", referencedColumnName = "customerID")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "employeeService", referencedColumnName = "employeeID")
    private Employee employeeService;

    @ManyToMany(mappedBy = "services")
    private Set<BankAccount> bankAccounts = new HashSet<>();

    public int getServiceNum() {
        return serviceNum;
    }

    public void setServiceNum(int serviceNum) {
        this.serviceNum = serviceNum;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public BranchOffice getBranchOffice() {
        return branchOffice;
    }

    public void setBranchOffice(BranchOffice branchOffice) {
        this.branchOffice = branchOffice;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployeeService() {
        return employeeService;
    }

    public void setEmployeeService(Employee employeeService) {
        this.employeeService = employeeService;
    }

    public Set<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }
}