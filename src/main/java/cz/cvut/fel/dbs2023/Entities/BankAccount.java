package cz.cvut.fel.dbs2023.Entities;
import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import cz.cvut.fel.dbs2023.Entities.Services;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "bankAccount")
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "accountNumber")
    private int accountNumber;


    @Column(name = "Type", length = 50, nullable = false)
    private String type;

    @Column(name = "balance", nullable = false)
    private int balance;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "bank_account_services",
            joinColumns = @JoinColumn(name = "account_number"),
            inverseJoinColumns = @JoinColumn(name = "service_num"))
    private Set<Services> services = new HashSet<>();



    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString(){
        return Integer.toString(accountNumber);
    }
}