package cz.cvut.fel.dbs2023.Entities;

import java.io.Serializable;
import java.util.Objects;

public class AddressId implements Serializable {
    private String street;
    private String city;

    public AddressId() {

    }

    public AddressId(String street, String city) {
        this.street = street;
        this.city = city;
    }
}