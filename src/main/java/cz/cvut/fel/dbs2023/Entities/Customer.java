package cz.cvut.fel.dbs2023.Entities;
import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customerID")
    private int customerID;

    @Column(name = "email", length = 50, nullable = false)
    private String email;

    @Column(name = "name", length = 70, nullable = false)
    private String name;

    @Column(name = "phone", length = 50, nullable = false)
    private String phone;

    @OneToOne
    @JoinColumn(name = "bankAccount", unique = true)
    private BankAccount bankAccount;

    @ManyToOne
    @JoinColumn(name = "attachedEmployee", referencedColumnName = "employeeID")
    private Employee attachedEmployee;

    @Column(name = "age", nullable = false)
    private int age;

    public Customer(){

    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCustomerID() {
        return customerID;
    }


    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return name;
    }

}