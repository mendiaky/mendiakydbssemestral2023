package cz.cvut.fel.dbs2023.Entities;
import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "branchOffice")
public class BranchOffice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "branchNum")
    private int branchNum;

    @Column(name = "capacity", nullable = false)
    private int capacity;

    @OneToOne
    @JoinColumns({
            @JoinColumn(name = "street", referencedColumnName = "street"),
            @JoinColumn(name = "city", referencedColumnName = "city")
    })
    private Address address;

    // Конструкторы, геттеры и сеттеры

    public int getBranchNum() {
        return branchNum;
    }

    public void setBranchNum(int branchNum) {
        this.branchNum = branchNum;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}