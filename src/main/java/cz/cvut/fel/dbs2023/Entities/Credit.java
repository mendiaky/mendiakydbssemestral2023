package cz.cvut.fel.dbs2023.Entities;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
@Entity
@DiscriminatorValue("Cred")
public class Credit extends Services implements Serializable {
    @Id
    @Column(name = "serviceNum")
    private int serviceNum;

    @Column(name = "period", nullable = false)
    private int period;

    @ManyToOne
    @JoinColumn(name = "branchOffice", referencedColumnName = "BranchNum")
    private BranchOffice branchOffice;

    @ManyToOne
    @JoinColumn(name = "customer", referencedColumnName = "customerID")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "employeeService", referencedColumnName = "employeeID")
    private Employee employeeService;

    public Credit(){}
}