package cz.cvut.fel.dbs2023.Entities;
import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "paymentCard")
public class PaymentCard {
    @Id
    @Column(name = "cardNumber")
    private int cardNumber;

    @Column(name = "Type", length = 50, nullable = false)
    private String type;

    @OneToOne
    @JoinColumn(name = "bankAccount", referencedColumnName = "accountNumber", unique = true)
    private BankAccount bankAccount;

    @ManyToOne
    @JoinColumn(name = "customer")
    private Customer customer;

}