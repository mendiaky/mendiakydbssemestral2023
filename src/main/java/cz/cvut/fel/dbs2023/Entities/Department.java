package cz.cvut.fel.dbs2023.Entities;
import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "departmentID")
    private int departmentID;

    @Column(name = "name", length = 50, unique = true, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "branchOffice", referencedColumnName = "branchNum")
    private BranchOffice branchOffice;

    @Column(name = "capacity", nullable = false)
    private int capacity;
}