package cz.cvut.fel.dbs2023.Entities;
import javax.persistence.*;
import java.util.List;
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employeeID")
    private int employeeID;

    @Column(name = "email", length = 50, nullable = false)
    private String email;

    @Column(name = "name", length = 70, nullable = false)
    private String name;

    @Column(name = "phone", length = 50, nullable = false)
    private String phone;

    @ManyToOne
    @JoinColumn(name = "employeeDepartment", referencedColumnName = "departmentID")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "customer")
    private Customer customer;

}