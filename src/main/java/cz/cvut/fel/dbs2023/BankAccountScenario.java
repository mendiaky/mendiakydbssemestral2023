package cz.cvut.fel.dbs2023;

import cz.cvut.fel.dbs2023.DAO.BankAccountDAO;
import cz.cvut.fel.dbs2023.Entities.BankAccount;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class BankAccountScenario {
    public void start(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("dbs2023-persistence-unit");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        // Creating a DAO object to work with BankAccount
        BankAccountDAO bankAccountDAO = new BankAccountDAO(entityManager);

        // Start of transaction
        EntityTransaction transaction = null;
        transaction = entityManager.getTransaction();

        transaction.begin();

        try {
            // Creating a new bank account
            BankAccount bankAccount = new BankAccount();
            bankAccount.setBalance(1000);
            bankAccount.setType("Standard");
            bankAccountDAO.create(bankAccount);

            // Getting an existing bank account by ID
            BankAccount existingAccount = bankAccountDAO.findById(bankAccount.getAccountNumber());
            System.out.println("Existing Bank Account: " + existingAccount);

            // Changing the balance of an existing bank account
            existingAccount.setBalance(2000);
            bankAccountDAO.update(existingAccount);

            // Deleting a bank account
            bankAccountDAO.delete(existingAccount);

            if (transaction != null && transaction.isActive()) {
                if (transaction.getRollbackOnly()) {
                    transaction.rollback();
                } else {
                    transaction.commit();
                }
            }

        } catch (Exception e) {
            // Exception handling and transaction rollback in case of an error
            if (transaction.isActive()) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            // Closing EntityManager and EntityManagerFactory
            entityManager.close();
        }
    }
}
